package actors

import actors.Message.Bidding
import akka.actor.{ActorLogging, ActorRef, FSM}

import scala.concurrent.duration.FiniteDuration

/**
  * Created by marcus on 1/08/17.
  */
class Item(name: String, duration: FiniteDuration, startValue: BigDecimal) extends FSM[State, Value] with ActorLogging {

  val historyRef: ActorRef = context.actorOf(Bids.props, name + "keep")
  context.watch(historyRef)

  startWith(Active, Value(startValue))

  when(Active){
    case Event(Bidding(amount), currentValue: Value) =>
      historyRef ! Append(sender, amount)
      if(amount > currentValue.amount) {
        stay replying Accepted using Value(amount)
      }
      else stay replying Rejected("Bid below current")
  }

  when(Ended) {
    case Event(Bidding(_), _) =>
    log.info("bid received while ended")
    stay replying Rejected("Auction ended")

  }

  whenUnhandled {
    case Event(EndNotify, _)            => goto(Ended)
    case Event(CurrentValue, hb: Value) => stay replying hb.amount
    case Event(GetHistory, _)           =>
      historyRef forward Retrieve
      stay
  }

  setTimer("EndAuction", EndNotify, duration)

}

sealed trait State
case object New extends State
case object Active extends State
case object Ended extends State

case class Value(amount: BigDecimal)

object Message {
  case class Bidding(amount: BigDecimal)
}

sealed trait BidStatus
case object Accepted extends BidStatus
case class Rejected(reason: String) extends BidStatus

case object EndNotify
case object CurrentValue

case object GetHistory