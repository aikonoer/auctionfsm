package actors

import akka.actor.{Actor, ActorLogging, ActorRef, Props}

/**
  * Created by marcus on 3/08/17.
  */
class Bids extends Actor with ActorLogging {

  var history: List[(ActorRef, BigDecimal)] = List[(ActorRef, BigDecimal)]()

  override def receive: Receive = {
    case a: Append   =>
      log.info("Received append message")
      history = history :+ (a.from, a.amount)
    case Retrieve =>
      log.info("Received retrieve message")
      sender ! history
  }
}

object Bids {
  def props: Props = Props(new Bids)
}

case class Append(from: ActorRef, amount: BigDecimal)
case object Retrieve
