package actors

import java.util.concurrent.TimeUnit

import actors.Message.Bidding
import akka.actor.ActorSystem
import akka.testkit.{TestActorRef, TestKit}
import org.scalatest.WordSpecLike

import scala.concurrent.duration.FiniteDuration

/**
  * Created by marcus on 2/08/17.
  */
class ItemTest extends TestKit(ActorSystem("testSystem"))
  with WordSpecLike
  with StopSystemAfterAll {

  "An Item Actor" must {
    "create new item for auction" in {

      val pen = TestActorRef(new Item("Pen", FiniteDuration(2, TimeUnit.SECONDS), BigDecimal(20)))
      Thread.sleep(3000)
      pen ! Bidding(30)
      expectMsg(Rejected("Auction ended"))
    }
  }

  "An item actor" must {
    "accept bid" in {
      val pen = TestActorRef(new Item("Pen", FiniteDuration(2, TimeUnit.SECONDS), BigDecimal(20)))
      Thread.sleep(1000)
      pen ! Bidding(21)
      expectMsg(Accepted)
    }
  }

  "An item actor" must {
    "accept multiple bids" in {
      val pen = TestActorRef(new Item("Pen", FiniteDuration(20, TimeUnit.SECONDS), BigDecimal(20)))
      pen ! Bidding(21)
      expectMsg(Accepted)
      pen ! Bidding(22)
      expectMsg(Accepted)
      pen ! Bidding(23)
      expectMsg(Accepted)
      pen ! CurrentValue
      expectMsg(BigDecimal(23))
    }
  }

}
