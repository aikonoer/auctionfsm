package actors

import akka.actor.ActorSystem
import akka.testkit.{TestActorRef, TestKit}
import org.scalatest.WordSpecLike

/**
  * Created by marcus on 3/08/17.
  */
class BidsTest extends TestKit(ActorSystem("testSystem"))
  with WordSpecLike
  with StopSystemAfterAll {

  "An actor" must {
    "keep history" in {
      val penBid = TestActorRef(Bids.props, "pen")
      val other = TestActorRef(Bids.props, "other")
      penBid ! Append(other, BigDecimal(30))
      expectNoMsg()
      penBid ! Retrieve
      expectMsg(List(other, BigDecimal(30)))
//      expectMsgType[List[(ActorRef, BigDecimal)]]
    }
  }

}
