package actors

import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest.{BeforeAndAfterAll, MustMatchers, Suite}

/**
  * Created by marcus on 2/08/17.
  */
trait StopSystemAfterAll extends BeforeAndAfterAll
  with ImplicitSender with MustMatchers {
  this: TestKit with Suite =>

  override protected def afterAll(): Unit = {
    super.afterAll()
    system.terminate()
  }
}
